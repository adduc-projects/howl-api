<?php

namespace Adduc\Howl;

use GuzzleHttp\Client;

class Api
{
    /**
     * @property string
     */
    protected $client_id;
    
    /**
     * @property array
     */
    protected $guzzle_options = [
        'base_uri' => "http://api.wolfpub.io/v2/",
        'connect_timeout' => 2,
        'timeout' => 10
    ];

    /**
     * @param string $client_id
     */
    public function __construct($client_id)
    {
        $this->client_id = $client_id;
    }

    /**
     * @return Entity\Network[]
     */
    public function getNetworks()
    {
        $data = $this->get('networks/all_shows');
        $networks = array();
        foreach ($data['networks'] as $network) {
            $networks[] = new Entity\Network($network);
        }
        return $networks;
    }

    /**
     * @param int $show_id
     * @return Entity\Show
     */
    public function getShow($show_id)
    {
        $show = $this->get("shows/{$show_id}");
        return new Entity\Show($show);
    }

    /**
     * @param int $show_id
     * @return Entity\Episode[]
     */
    public function getEpisodes($show_id)
    {
        $data = $this->get("shows/{$show_id}/episodes/");
        $episodes = array();
        foreach ($data['episodes'] as $episode) {
            $episodes[] = new Entity\Episode($episode);
        }
        return $episodes;
    }

    /**
     * @param int $episode_id
     * @return Entity\Episode
     */
    public function getEpisode($episode_id)
    {
        $episode = $this->get("shows/-1/episodes/{$episode_id}");
        return new Entity\Episode($episode);
    }

    /**
     * @return Entity\Network[]
     */
    public function getRecentEpisodes()
    {
        $data = $this->get('networks/recent');
        $networks = array();
        foreach ($data['networks'] as $network) {
            $networks[] = new Entity\Network($network);
        }
        
        return $networks;
    }

    /**
     * @param string $url
     * @param array $data
     * @param array $options
     * @return array
     */
    protected function get($url, array $data = [], array $options = [])
    {
        $options += $this->guzzle_options;
        
        $client = new Client($options);

        $data += array(
            'client_id' => $this->client_id,
            'android' => 'true'
        );

        $response = $client->request('GET', $url, ['query' => $data]);
        $data = json_decode($response->getBody()->getContents(), true);

        if (empty($data) || isset($data['success']) && !empty($data['success'])) {
            error_log($response->getBody());
            throw new \Exception();
        }

        return $data;
    }
}

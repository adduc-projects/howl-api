<?php

namespace Adduc\Howl\Entity;

use DateTime;
use stdClass;

class Episode extends Entity
{
    /** @property int */
    public $id;

    /** @property string */
    public $name;

    /** @property string */
    public $description;

    /** @property int */
    public $show_id;

    /** @property int */
    public $episode_number;

    /** @property string */
    public $duration;

    /** @property string */
    public $streaming_url;

    /** @property string */
    public $streaming_url_large;

    public $ad_free_url;
    public $ad_free_streaming_url;

    /** @property string */
    public $mp3_url;
    
    /** @property int */
    public $is_published;
    
    /** @property DateTime */
    public $published_at;

    /** @property string */
    public $earwolf_v1_url;

    /** @property string */
    public $guest_names;

    public $hashtag;

    /** @property int */
    public $tier_required;

    /** @property int */
    public $soundcloud_id;

    public $stagebloc_id;

    /** @property Photo */
    public $guest_photos;

    /** @property Photo */
    public $gallery_photos;

    /** @property string */
    public $forum_topic_url;
    
    public $windowed;
    public $soundcloud_url;

    /** @property int */
    public $published;

    /** @property DateTime */
    public $created_at;

    /** @property DateTime */
    public $updated_at;

    /** @property sting */
    public $url_slug;

    /** @property int */
    public $guest_photos_count;

    /** @property int */
    public $gallery_photos_count;

    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->is_published = !!$this->published_at;
        $this->published_at = new DateTime($this->published_at);
        $this->created_at = new DateTime($this->created_at);
        $this->updated_at = new DateTime($this->updated_at);
        foreach ($this->guest_photos ?: [] as $key => $guest_photo) {
            $this->guest_photos[$key] = new Photo($guest_photo);
        }
        foreach ($this->gallery_photos ?: [] as $key => $gallery_photo) {
            $this->gallery_photos[$key] = new Photo($gallery_photo);
        }
    }

    /**
     * Determines the highest-quality audio available.
     *
     * @return string
     */
    public function getAudioUrl()
    {

        if ($this->ad_free_url) {
            $return = $this->ad_free_url;
        } elseif ($this->ad_free_streaming_url) {
            $return = $this->ad_free_streaming_url;
        } elseif ($this->streaming_url_large) {
            $return = $this->streaming_url_large;
        } elseif ($this->streaming_url) {
            $return = $this->streaming_url;
        } else {
            $return = $this->mp3_url;
        }

        // iTunes is a little odd with 302 redirects, which Soundcloud
        // sends from some URLs. When we detect a Soundcloud URL,
        // override and use a known 301-redirect emitting URL.
        if (stripos($return, 'soundcloud.com') !== false && $this->soundcloud_id) {
            $return = 'http://feeds.soundcloud.com/stream/' . $this->soundcloud_id . '.mp3';
        }

        return $return;
    }
}

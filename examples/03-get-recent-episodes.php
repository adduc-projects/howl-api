<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

$api = new Adduc\Howl\Api($client_id);

// Get a list of recent episodes from all shows
$episodes = $api->getRecentEpisodes();

echo "<pre>";
print_r($episodes);
